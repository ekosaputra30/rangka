import { gsap } from 'gsap'
import { ScrollTrigger } from "gsap/src/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)

let thumbPopup__1 = document.querySelector('.thumbnail--pagefour .thumbnail--popup img.popup--1')
let thumbPopup__2 = document.querySelector('.thumbnail--pagefour .thumbnail--popup img.popup--2')
let thumbPopup__4 = document.querySelector('.thumbnail--pagefour .thumbnail--popup img.popup--4')
let thumbPopup__5 = document.querySelector('.thumbnail--pagefour .thumbnail--popup img.popup--5')
let thumbPopups = document.querySelectorAll('.thumbnail--pagefour .thumbnail--popup img.popups')
let trigger__pagethree = document.querySelector('.thumbnail--pagethree')

gsap.set(thumbPopup__1, {
  xPercent: 23,
  yPercent: 0,
})
gsap.set(thumbPopup__2, {
  xPercent: 13,
  yPercent: 0,
})
gsap.set(thumbPopup__4, {
  xPercent: -17,
  yPercent: 16
})
gsap.set(thumbPopup__5, {
  xPercent: -20,
  yPercent: 5,
})

gsap.utils.toArray(thumbPopups).forEach((elm, i) => {
  gsap.to(elm, {
    xPercent: 0,
    yPercent: 0,
    scrollTrigger: {
      trigger: trigger__pagethree,
      start: 'top top',
      end: '50% bottom',
      markers: (DEBUG) ? true : false,
      scrub: 1.05,
      pinSpacing: false
    }
  })
})

