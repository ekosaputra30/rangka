import { getBrowser } from './detect-browser'
import CanvasSlideshow from './canvasslideshow'
// import LocomotiveScroll from 'locomotive-scroll';
import { gsap } from 'gsap'
import { ScrollTrigger } from "gsap/src/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger)

let md = getBrowser();
let spriteImagesSrc = []
let navbar = document.querySelector(".navbar");
let dataPage = document.querySelector(".app__wrapper").getAttribute('data-page');
let liquid        = document.getElementById('liquid')
let liquidcanvas  = document.querySelector('#liquid canvas')
let words         = document.querySelectorAll('.word-wrap > .word')
let wordline      = document.querySelector('.word-wrap.line')
let banner        = document.querySelector('.banner')
let buttondelay   = document.querySelector('.button--delay')
let drw           = document.querySelector('.drawer')
let drwrapper     = document.querySelector('.drawer__wrapper')
let plx_hand      = document.querySelector('.with-hand')
let worksCover    = document.querySelectorAll('.work__cover')
let worksBoxed    = document.querySelectorAll('.work--boxed')
let worksBox      = document.querySelector('.work__box.d-lg-flex')
let preloader = document.querySelector('.preloader')
let recentBoxed   = 0
let recentBox     = 0

const navbarTrans = gsap.from(navbar, {
	backgroundColor: '#323233',
	paused: true,
	duration: 0.2,
}).progress(1)

ScrollTrigger.create({
	start: 'top top',
	end: function end() {
		return `${window.innerHeight} top`
	},
	onUpdate: (self) => {
		let pgrs = self.progress.toFixed(2)
		let drcn = self.direction

		if (drcn === -1) {
			navbarTrans.play()
		} else {
			if (pgrs >= 0.80) navbarTrans.reverse()
		}
	}
})

if (words) {
	words.forEach(function(word, index) {
		word.classList.add('loaded')
		word.classList.add('loaded-'+index)
	})
}

if (buttondelay) buttondelay.classList.add('loaded')

drw.onclick = function() {
	let bd = document.body
	if (bd.classList.contains('menu-is-open')) {
		bd.classList.remove('menu-is-open')
		drwrapper.classList.remove('is-open')
	}
	else {
		bd.classList.add('menu-is-open')
		drwrapper.classList.add('is-open')
	}
}

// let containerLoc = document.querySelector('[data-scroll-container]')
// if (containerLoc) {
// 	const scroll = new LocomotiveScroll({
// 		el: containerLoc,
// 		smooth: true,
// 		repeat: true
// 	})

// 	scroll.on('call', (t, e, n) => {
// 		if (t === 'navbarScrollRest' && e === 'exit') {
// 			navbar.style.backgroundColor = "#323233"
// 		}
// 		if (t === 'navbarScrollRest' && e === 'enter') {
// 			navbar.style.backgroundColor = "transparent"
// 		}
// 	})
// 	window.addEventListener("resize", function(){ scroll.update(); });
// }
function animateValue(start, end, duration, pre) {
	let range = end - start,
	current = start,
	increment = end > start? 1 : -1,
	stepTime = Math.abs(Math.floor(duration / range));
    
	let timer = setInterval(function() {
		current += increment;
        
		if (current == end) {
            clearInterval(timer);
			pre.classList.add('loaded')

			document.querySelector('header').classList.add('done-loading')
			document.querySelector('#app').classList.add('done-loading')
			document.querySelector('.drawer').classList.add('done-loading')
		}
	}, stepTime);
}
window.onload = function() {
	setTimeout(() => {
			let perf = window.performance.timing;
			let estIme = (perf.loadEventEnd - perf.navigationStart)
			let time = parseInt((estIme/1000)%60)*100;    
			let start = 0
			let end = 100
			let durataion = time

			animateValue(start, end, durataion, preloader)
	}, 0);
	if(worksBoxed.length > 0) {
		Array.from(worksCover).forEach(function(workboxed, ind) {

			gsap.from(workboxed.querySelector('img'), {
				ease: 'none',
				yPercent: -20
			})

			gsap.to(workboxed.querySelector('img'), {
				scrollTrigger: {
					trigger: document.querySelector('.intro__works'),
					markers: false,
					scrub: true,
					pinSpacing: false
				},
				ease: 'none',
				yPercent: 20
			})
		})
	}
	if (dataPage == 'home') {
		// if (banner) {
		// 	banner.addEventListener('mousemove', (e) => {
		// 		const spd = 2;
		// 		const x   = (window.innerWidth - e.pageX*spd)/100
		// 		const y   = (window.innerHeight - e.pageY*spd)/100
	
		// 		liquidcanvas.style.transform = `scale(1) translateX(${x}px) translateY(${y}px)`
		// 	})
		// }

		if (liquid) liquid.classList.add('is-loaded')

		let displacementImage
		if (DEBUG) {
			spriteImagesSrc.push(`${baseUrl}/assets/images/bg/bg-wave.png`);
			displacementImage = `${baseUrl}/assets/images/pattern-clouds.jpg`
		} else { 
			spriteImagesSrc.push(`${baseUrl}/themes/rangka/assets/images/bg/bg-wave.png`);
			displacementImage = `${baseUrl}/themes/rangka/assets/images/pattern-clouds.jpg`
		}
		
		let initCanvasSlideshow = new CanvasSlideshow({
			sprites: spriteImagesSrc,
			displacementImage: displacementImage,
			autoPlay: true,
			autoPlaySpeed: [0, 3],
			interactive: false,
			interactionEvent: 'click', // 'click', 'hover', 'both' 
			displaceAutoFit: false,
			dispatchPointerOver: true // restarts pointerover event after click 
		})

		if(worksBoxed.length > 0) {

			worksBoxed.forEach(function(workboxed, ind) {
				if (ind < 2) {
					let t = workboxed.clientHeight
					let c = window.getComputedStyle(workboxed)
					let mb = parseInt(c.marginTop, 10);
					let my = mb*2
					let outerHeight = t+my
					
					recentBoxed += outerHeight
				}
			})

			if(worksBox) {
				let t = worksBox.clientHeight
				let c = window.getComputedStyle(worksBox)
				let mb = 32;
				let my = mb*2
				let outerHeight = t+my
				
				recentBox = outerHeight
			}
			if (md.device == 'Desktop') {
				let t = recentBoxed+recentBox
				document.querySelector('.intro__works-recent').style.maxHeight = t+'px'
			}
		}
	}
}

document.addEventListener('click', function(event) {
	let isDrawerInside = drw.contains(event.target)

	if (!isDrawerInside) {
		let bd = document.body
		if (bd.classList.contains('menu-is-open')) {
			bd.classList.remove('menu-is-open')
			drwrapper.classList.remove('is-open')
		}
	}
})
