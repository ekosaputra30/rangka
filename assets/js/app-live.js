parcelRequire = function(e, r, t, n) {
    var i, o = "function" == typeof parcelRequire && parcelRequire,
        u = "function" == typeof require && require;

    function f(t, n) {
        if (!r[t]) {
            if (!e[t]) {
                var i = "function" == typeof parcelRequire && parcelRequire;
                if (!n && i) return i(t, !0);
                if (o) return o(t, !0);
                if (u && "string" == typeof t) return u(t);
                var c = new Error("Cannot find module '" + t + "'");
                throw c.code = "MODULE_NOT_FOUND", c
            }
            p.resolve = function(r) {
                return e[t][1][r] || r
            }, p.cache = {};
            var l = r[t] = new f.Module(t);
            e[t][0].call(l.exports, p, l, l.exports, this)
        }
        return r[t].exports;

        function p(e) {
            return f(p.resolve(e))
        }
    }
    f.isParcelRequire = !0, f.Module = function(e) {
        this.id = e, this.bundle = f, this.exports = {}
    }, f.modules = e, f.cache = r, f.parent = o, f.register = function(r, t) {
        e[r] = [function(e, r) {
            r.exports = t
        }, {}]
    };
    for (var c = 0; c < t.length; c++) try {
        f(t[c])
    } catch (e) {
        i || (i = e)
    }
    if (t.length) {
        var l = f(t[t.length - 1]);
        "object" == typeof exports && "undefined" != typeof module ? module.exports = l : "function" == typeof define && define.amd ? define(function() {
            return l
        }) : n && (this[n] = l)
    }
    if (parcelRequire = f, i) throw i;
    return f
}({
    "tOor": [function(require, module, exports) {
        var define;
        var t;
        ! function(e, n) {
            "object" == typeof exports && "object" == typeof module ? module.exports = n() : "function" == typeof t && t.amd ? t("simpleParallax", [], n) : "object" == typeof exports ? exports.simpleParallax = n() : e.simpleParallax = n()
        }(window, function() {
            return function(t) {
                var e = {};

                function n(i) {
                    if (e[i]) return e[i].exports;
                    var s = e[i] = {
                        i: i,
                        l: !1,
                        exports: {}
                    };
                    return t[i].call(s.exports, s, s.exports, n), s.l = !0, s.exports
                }
                return n.m = t, n.c = e, n.d = function(t, e, i) {
                    n.o(t, e) || Object.defineProperty(t, e, {
                        enumerable: !0,
                        get: i
                    })
                }, n.r = function(t) {
                    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                        value: "Module"
                    }), Object.defineProperty(t, "__esModule", {
                        value: !0
                    })
                }, n.t = function(t, e) {
                    if (1 & e && (t = n(t)), 8 & e) return t;
                    if (4 & e && "object" == typeof t && t && t.__esModule) return t;
                    var i = Object.create(null);
                    if (n.r(i), Object.defineProperty(i, "default", {
                            enumerable: !0,
                            value: t
                        }), 2 & e && "string" != typeof t)
                        for (var s in t) n.d(i, s, function(e) {
                            return t[e]
                        }.bind(null, s));
                    return i
                }, n.n = function(t) {
                    var e = t && t.__esModule ? function() {
                        return t.default
                    } : function() {
                        return t
                    };
                    return n.d(e, "a", e), e
                }, n.o = function(t, e) {
                    return Object.prototype.hasOwnProperty.call(t, e)
                }, n.p = "", n(n.s = 0)
            }([function(t, e, n) {
                "use strict";
                n.r(e), n.d(e, "default", function() {
                    return w
                });
                var i = function() {
                    return Element.prototype.closest && "IntersectionObserver" in window
                };

                function s(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var i = e[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
                    }
                }
                var o = new(function() {
                        function t() {
                            ! function(t, e) {
                                if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
                            }(this, t), this.positions = {
                                top: 0,
                                bottom: 0,
                                height: 0
                            }
                        }
                        var e, n;
                        return e = t, (n = [{
                            key: "setViewportTop",
                            value: function(t) {
                                return this.positions.top = t ? t.scrollTop : window.pageYOffset, this.positions
                            }
                        }, {
                            key: "setViewportBottom",
                            value: function() {
                                return this.positions.bottom = this.positions.top + this.positions.height, this.positions
                            }
                        }, {
                            key: "setViewportAll",
                            value: function(t) {
                                return this.positions.top = t ? t.scrollTop : window.pageYOffset, this.positions.height = t ? t.clientHeight : document.documentElement.clientHeight, this.positions.bottom = this.positions.top + this.positions.height, this.positions
                            }
                        }]) && s(e.prototype, n), t
                    }()),
                    r = function(t) {
                        return NodeList.prototype.isPrototypeOf(t) || HTMLCollection.prototype.isPrototypeOf(t) ? Array.from(t) : "string" == typeof t || t instanceof String ? document.querySelectorAll(t) : [t]
                    },
                    a = function() {
                        for (var t, e = "transform webkitTransform mozTransform oTransform msTransform".split(" "), n = 0; void 0 === t;) t = void 0 !== document.createElement("div").style[e[n]] ? e[n] : void 0, n += 1;
                        return t
                    }(),
                    l = function(t) {
                        return "img" !== t.tagName.toLowerCase() && "picture" !== t.tagName.toLowerCase() || !!t && !!t.complete && (void 0 === t.naturalWidth || 0 !== t.naturalWidth)
                    };

                function u(t, e) {
                    (null == e || e > t.length) && (e = t.length);
                    for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                    return i
                }

                function c(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var i = e[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
                    }
                }
                var h = function() {
                    function t(e, n) {
                        var i = this;
                        ! function(t, e) {
                            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
                        }(this, t), this.element = e, this.elementContainer = e, this.settings = n, this.isVisible = !0, this.isInit = !1, this.oldTranslateValue = -1, this.init = this.init.bind(this), this.customWrapper = this.settings.customWrapper && this.element.closest(this.settings.customWrapper) ? this.element.closest(this.settings.customWrapper) : null, l(e) ? this.init() : this.element.addEventListener("load", function() {
                            setTimeout(function() {
                                i.init(!0)
                            }, 50)
                        })
                    }
                    var e, n;
                    return e = t, (n = [{
                        key: "init",
                        value: function(t) {
                            var e = this;
                            this.isInit || (t && (this.rangeMax = null), this.element.closest(".simpleParallax") || (!1 === this.settings.overflow && this.wrapElement(this.element), this.setTransformCSS(), this.getElementOffset(), this.intersectionObserver(), this.getTranslateValue(), this.animate(), this.settings.delay > 0 ? setTimeout(function() {
                                e.setTransitionCSS(), e.elementContainer.classList.add("simple-parallax-initialized")
                            }, 10) : this.elementContainer.classList.add("simple-parallax-initialized"), this.isInit = !0))
                        }
                    }, {
                        key: "wrapElement",
                        value: function() {
                            var t = this.element.closest("picture") || this.element,
                                e = this.customWrapper || document.createElement("div");
                            e.classList.add("simpleParallax"), e.style.overflow = "hidden", this.customWrapper || (t.parentNode.insertBefore(e, t), e.appendChild(t)), this.elementContainer = e
                        }
                    }, {
                        key: "unWrapElement",
                        value: function() {
                            var t = this.elementContainer;
                            this.customWrapper ? (t.classList.remove("simpleParallax"), t.style.overflow = "") : t.replaceWith.apply(t, function(t) {
                                return function(t) {
                                    if (Array.isArray(t)) return u(t)
                                }(t) || function(t) {
                                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t)
                                }(t) || function(t, e) {
                                    if (t) {
                                        if ("string" == typeof t) return u(t, e);
                                        var n = Object.prototype.toString.call(t).slice(8, -1);
                                        return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? Array.from(t) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? u(t, e) : void 0
                                    }
                                }(t) || function() {
                                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                                }()
                            }(t.childNodes))
                        }
                    }, {
                        key: "setTransformCSS",
                        value: function() {
                            !1 === this.settings.overflow && (this.element.style[a] = "scale(".concat(this.settings.scale, ")")), this.element.style.willChange = "transform"
                        }
                    }, {
                        key: "setTransitionCSS",
                        value: function() {
                            this.element.style.transition = "transform ".concat(this.settings.delay, "s ").concat(this.settings.transition)
                        }
                    }, {
                        key: "unSetStyle",
                        value: function() {
                            this.element.style.willChange = "", this.element.style[a] = "", this.element.style.transition = ""
                        }
                    }, {
                        key: "getElementOffset",
                        value: function() {
                            var t = this.elementContainer.getBoundingClientRect();
                            if (this.elementHeight = t.height, this.elementTop = t.top + o.positions.top, this.settings.customContainer) {
                                var e = this.settings.customContainer.getBoundingClientRect();
                                this.elementTop = t.top - e.top + o.positions.top
                            }
                            this.elementBottom = this.elementHeight + this.elementTop
                        }
                    }, {
                        key: "buildThresholdList",
                        value: function() {
                            for (var t = [], e = 1; e <= this.elementHeight; e++) {
                                var n = e / this.elementHeight;
                                t.push(n)
                            }
                            return t
                        }
                    }, {
                        key: "intersectionObserver",
                        value: function() {
                            var t = {
                                root: null,
                                threshold: this.buildThresholdList()
                            };
                            this.observer = new IntersectionObserver(this.intersectionObserverCallback.bind(this), t), this.observer.observe(this.element)
                        }
                    }, {
                        key: "intersectionObserverCallback",
                        value: function(t) {
                            var e = this;
                            t.forEach(function(t) {
                                t.isIntersecting ? e.isVisible = !0 : e.isVisible = !1
                            })
                        }
                    }, {
                        key: "checkIfVisible",
                        value: function() {
                            return this.elementBottom > o.positions.top && this.elementTop < o.positions.bottom
                        }
                    }, {
                        key: "getRangeMax",
                        value: function() {
                            var t = this.element.clientHeight;
                            this.rangeMax = t * this.settings.scale - t
                        }
                    }, {
                        key: "getTranslateValue",
                        value: function() {
                            var t = ((o.positions.bottom - this.elementTop) / ((o.positions.height + this.elementHeight) / 100)).toFixed(1);
                            return t = Math.min(100, Math.max(0, t)), 0 !== this.settings.maxTransition && t > this.settings.maxTransition && (t = this.settings.maxTransition), this.oldPercentage !== t && (this.rangeMax || this.getRangeMax(), this.translateValue = (t / 100 * this.rangeMax - this.rangeMax / 2).toFixed(0), this.oldTranslateValue !== this.translateValue && (this.oldPercentage = t, this.oldTranslateValue = this.translateValue, !0))
                        }
                    }, {
                        key: "animate",
                        value: function() {
                            var t, e = 0,
                                n = 0;
                            (this.settings.orientation.includes("left") || this.settings.orientation.includes("right")) && (n = "".concat(this.settings.orientation.includes("left") ? -1 * this.translateValue : this.translateValue, "px")), (this.settings.orientation.includes("up") || this.settings.orientation.includes("down")) && (e = "".concat(this.settings.orientation.includes("up") ? -1 * this.translateValue : this.translateValue, "px")), t = !1 === this.settings.overflow ? "translate3d(".concat(n, ", ").concat(e, ", 0) scale(").concat(this.settings.scale, ")") : "translate3d(".concat(n, ", ").concat(e, ", 0)"), this.element.style[a] = t
                        }
                    }]) && c(e.prototype, n), t
                }();

                function f(t) {
                    return function(t) {
                        if (Array.isArray(t)) return p(t)
                    }(t) || function(t) {
                        if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t)
                    }(t) || m(t) || function() {
                        throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                    }()
                }

                function m(t, e) {
                    if (t) {
                        if ("string" == typeof t) return p(t, e);
                        var n = Object.prototype.toString.call(t).slice(8, -1);
                        return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? Array.from(t) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? p(t, e) : void 0
                    }
                }

                function p(t, e) {
                    (null == e || e > t.length) && (e = t.length);
                    for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                    return i
                }

                function d(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var i = e[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
                    }
                }
                var y, v, g = !1,
                    b = [],
                    w = function() {
                        function t(e, n) {
                            if (function(t, e) {
                                    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
                                }(this, t), e && i()) {
                                if (this.elements = r(e), this.defaults = {
                                        delay: 0,
                                        orientation: "up",
                                        scale: 1.3,
                                        overflow: !1,
                                        transition: "cubic-bezier(0,0,0,1)",
                                        customContainer: "",
                                        customWrapper: "",
                                        maxTransition: 0
                                    }, this.settings = Object.assign(this.defaults, n), this.settings.customContainer) {
                                    var s = function(t, e) {
                                        return function(t) {
                                            if (Array.isArray(t)) return t
                                        }(t) || function(t, e) {
                                            if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) {
                                                var n = [],
                                                    i = !0,
                                                    s = !1,
                                                    o = void 0;
                                                try {
                                                    for (var r, a = t[Symbol.iterator](); !(i = (r = a.next()).done) && (n.push(r.value), !e || n.length !== e); i = !0);
                                                } catch (t) {
                                                    s = !0, o = t
                                                } finally {
                                                    try {
                                                        i || null == a.return || a.return()
                                                    } finally {
                                                        if (s) throw o
                                                    }
                                                }
                                                return n
                                            }
                                        }(t, e) || m(t, e) || function() {
                                            throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                                        }()
                                    }(r(this.settings.customContainer), 1);
                                    this.customContainer = s[0]
                                }
                                this.lastPosition = -1, this.resizeIsDone = this.resizeIsDone.bind(this), this.refresh = this.refresh.bind(this), this.proceedRequestAnimationFrame = this.proceedRequestAnimationFrame.bind(this), this.init()
                            }
                        }
                        var e, n;
                        return e = t, (n = [{
                            key: "init",
                            value: function() {
                                var t = this;
                                o.setViewportAll(this.customContainer), b = [].concat(f(this.elements.map(function(e) {
                                    return new h(e, t.settings)
                                })), f(b)), g || (this.proceedRequestAnimationFrame(), window.addEventListener("resize", this.resizeIsDone), g = !0)
                            }
                        }, {
                            key: "resizeIsDone",
                            value: function() {
                                clearTimeout(v), v = setTimeout(this.refresh, 200)
                            }
                        }, {
                            key: "proceedRequestAnimationFrame",
                            value: function() {
                                var t = this;
                                o.setViewportTop(this.customContainer), this.lastPosition !== o.positions.top ? (o.setViewportBottom(), b.forEach(function(e) {
                                    t.proceedElement(e)
                                }), y = window.requestAnimationFrame(this.proceedRequestAnimationFrame), this.lastPosition = o.positions.top) : y = window.requestAnimationFrame(this.proceedRequestAnimationFrame)
                            }
                        }, {
                            key: "proceedElement",
                            value: function(t) {
                                (this.customContainer ? t.checkIfVisible() : t.isVisible) && t.getTranslateValue() && t.animate()
                            }
                        }, {
                            key: "refresh",
                            value: function() {
                                o.setViewportAll(this.customContainer), b.forEach(function(t) {
                                    t.getElementOffset(), t.getRangeMax()
                                }), this.lastPosition = -1
                            }
                        }, {
                            key: "destroy",
                            value: function() {
                                var t = this,
                                    e = [];
                                b = b.filter(function(n) {
                                    return t.elements.includes(n.element) ? (e.push(n), !1) : n
                                }), e.forEach(function(e) {
                                    e.unSetStyle(), !1 === t.settings.overflow && e.unWrapElement()
                                }), b.length || (window.cancelAnimationFrame(y), window.removeEventListener("resize", this.refresh), g = !1)
                            }
                        }]) && d(e.prototype, n), t
                    }()
            }]).default
        });
    }, {}],
    "kR2E": [function(require, module, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.getBrowser = A;
        var e, t, n, r, o, a, i, s, g, d, u, v, p, c, f = [],
            l = [];

        function A() {
            p = null == document.referer ? "N/A" : document.referer, a = 1 == /Mobi/.test(navigator.userAgent) ? "Mobile" : "Desktop";
            var e = !!window.opr && !!opr.addons || !!window.opera || navigator.userAgent.indexOf(" OPR/") >= 0,
                r = "undefined" != typeof InstallTrigger,
                o = /constructor/i.test(window.HTMLElement) || "[object SafariRemoteNotification]" === (!window.safari || safari.pushNotification).toString(),
                i = !!document.documentMode,
                s = !i && !!window.StyleMedia,
                g = !!window.chrome && !!window.chrome.webstore;
            if (1 == e ? t = "Opera" : 1 == r ? t = "FireFox" : 1 == o ? t = "Safari" : 1 == i ? t = "Internet Explorer" : 1 == s ? t = "Microsoft Edge" : 1 == g && (t = "Chrome"), "Desktop" == a) 1 == /Windows/.test(navigator.userAgent) ? (n = "Windows", 1 == /5.1;/.test(navigator.userAgent) ? n += " XP" : 1 == /6.0;/.test(navigator.userAgent) ? n += " Vista" : 1 == /6.1;/.test(navigator.userAgent) ? n += " 7" : 1 == /6.2/.test(navigator.userAgent) ? n += " 8" : 1 == /10.0;/.test(navigator.userAgent) && (n += " 10"), 1 == /64/.test(navigator.userAgent) ? n += " 64-bit" : n += " 32-bit") : 1 == /Macintosh/.test(navigator.userAgent) && (n = "Macintosh", 1 == /OS X/.test(navigator.userAgent) && (n += " OS X"));
            else if ("Mobile" == a)
                if (1 == /Windows/.test(navigator.userAgent)) n = "Windows", 1 == /Phone 8.0/.test(navigator.userAgent) ? n += " Phone 8.0" : 1 == /Phone 10.0/.test(navigator.userAgent) && (n += " Phone 10.0");
                else if (1 == /Android/.test(navigator.userAgent)) {
                var d = function() {
                    if (/Android/.test(navigator.appVersion)) return navigator.appVersion.match(/Android (\d+).(\d+)/)
                }();
                n = d[0]
            } else if (1 == /iPhone;/.test(navigator.userAgent)) {
                function u() {
                    if (/iP(hone|od|ad)/.test(navigator.appVersion)) {
                        var e = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
                        return [parseInt(e[1], 10), parseInt(e[2], 10), parseInt(e[3] || 0, 10)]
                    }
                }
                d = u();
                n = "iOS " + d[0] + "." + d[1] + "." + d[2]
            } else if (1 == /iPad;/.test(navigator.userAgent)) {
                function u() {
                    if (/iP(hone|od|ad)/.test(navigator.appVersion)) {
                        var e = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
                        return [parseInt(e[1], 10), parseInt(e[2], 10), parseInt(e[3] || 0, 10)]
                    }
                }
                d = u();
                n = "iOS " + d[0] + "." + d[1] + "." + d[2]
            } else 1 == /BBd*/.test(navigator.userAgent) && (n = "BlackBerry");
            return f = {
                language: navigator.language,
                languages: navigator.languages,
                user_agent: navigator.userAgent,
                browser: t,
                device: a,
                referer: p,
                os: n,
                online: navigator.onLine,
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                screen_resolution: screen.width + " x " + screen.height,
                cookie_enabled: navigator.cookieEnabled
            }
        }
    }, {}],
    "baIT": [function(require, module, exports) {
        ! function(e, t, i) {
            "use strict";
            var o;
            o = function(i, o) {
                var s, n, r, f, h, g;
                return h = 0, g = 0, n = 0, r = {}, f = [], 0, (s = function(t, i) {
                    for (n in this.options = {
                            speed: 1,
                            boost: 0
                        }, i) this.options[n] = i[n];
                    if ((this.options.speed < 0 || this.options.speed > 1) && (this.options.speed = 1), t || (t = "paraxify"), e.getElementsByClassName(t.replace(".", ""))) this.photos = e.getElementsByClassName(t.replace(".", ""));
                    else {
                        if (!1 === e.querySelector(t)) throw new Error("The elements you're trying to select don't exist.");
                        this.photos = querySelector(t)
                    }
                    r = this.options, f = this.photos, this._init(this)
                }).prototype = {
                    update: function() {
                        for (g = t.innerHeight, n = 0; n < f.length;) f[n].style.backgroundPosition = "1rem center", f[n].url = t.getComputedStyle(f[n], !1).backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, "$2").split(",")[0], f[n].img || (f[n].img = new Image), f[n].url !== f[n].img.src && (this._check(n), f[n].img.src = f[n].url), n++;
                        this._animate()
                    },
                    _init: function() {
                        this.update(), t.onscroll = function() {
                            this._animate()
                        }.bind(this), t.onresize = function() {
                            this.update()
                        }.bind(this)
                    },
                    _check: function(e) {
                        var i, o;
                        (o = f[e]).ok = !0, o.bgSize = t.getComputedStyle(o, !1).backgroundSize, i = g;
                        var s = this;
                        f[e].img.onload = function() {
                            if (console.log("main ", o), "" === o.bgSize || "auto" === o.bgSize) {
                                if (this.height < o.offsetHeight) throw o.ok = !1, new Error("The image " + o.url + " (" + this.height + "px) is too short for that container (" + o.offsetHeight + "px).");
                                i = this.height, this.height < g && (i += (g - o.offsetHeight) * r.speed)
                            } else if ("cover" === o.bgSize) {
                                if (g < o.offsetHeight) throw o.ok = !1, f[e].style.backgroundPosition = "1rem top", new Error("The container (" + o.offsetHeight + "px) can't be bigger than the image (" + g + "px).")
                            } else t.getComputedStyle(o, !1).backgroundSize, s._check(e);
                            i !== o.offsetHeight ? o.diff = -(i - o.offsetHeight) : o.diff = -i, o.diff *= r.speed, o.diff = o.diff - o.offsetHeight * r.boost
                        }
                    },
                    _visible: function(e) {
                        return h + g > f[e].offsetTop && h < f[e].offsetTop + f[e].offsetHeight
                    },
                    _animate: function() {
                        var i, o;
                        for (h = void 0 !== t.pageYOffset ? t.pageYOffset : (e.documentElement || e.body.parentNode || e.body).scrollTop, n = 0; n < f.length;) this._check(n), f[n].ok && "fixed" === t.getComputedStyle(f[n], !1).backgroundAttachment && this._visible(n) ? (i = (h - f[n].offsetTop + g) / (f[n].offsetHeight + g), o = f[n].diff * (i - .5), "cover" !== f[n].bgSize && (o += (g - f[n].img.height) / 2), o = Math.round(100 * o) / 100) : o = "center", f[n].style.backgroundPosition = "1rem " + o + "px", n++
                    }
                }, new s(i, o)
            }, t.paraxify = o, module.exports.paraxify = o
        }(document, window);
    }, {}],
    "JAKd": [function(require, module, exports) {
        "use strict";
        var e, t = a(require("simple-parallax-js")),
            r = require("./detect-browser");

        function a(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        module.hot && module.hot.accept(), window.CanvasSlideshow = function(e) {
            var t = this;
            (e = e || {}).stageWidth = e.hasOwnProperty("stageWidth") ? e.stageWidth : 1920, e.stageHeight = e.hasOwnProperty("stageHeight") ? e.stageHeight : 1080, e.pixiSprites = e.hasOwnProperty("sprites") ? e.sprites : [], e.centerSprites = !!e.hasOwnProperty("centerSprites") && e.centerSprites, e.autoPlay = !e.hasOwnProperty("autoPlay") || e.autoPlay, e.autoPlaySpeed = e.hasOwnProperty("autoPlaySpeed") ? e.autoPlaySpeed : [10, 3], e.fullScreen = !e.hasOwnProperty("fullScreen") || e.fullScreen, e.displacementImage = e.hasOwnProperty("displacementImage") ? e.displacementImage : "", e.displaceAutoFit = !!e.hasOwnProperty("displaceAutoFit") && e.displaceAutoFit, e.wacky = !!e.hasOwnProperty("wacky") && e.wacky, e.interactive = !!e.hasOwnProperty("interactive") && e.interactive, e.interactionEvent = e.hasOwnProperty("interactionEvent") ? e.interactionEvent : "", e.displacementCenter = !!e.hasOwnProperty("displacementCenter") && e.displacementCenter, e.dispatchPointerOver = !!e.hasOwnProperty("dispatchPointerOver") && e.dispatchPointerOver;
            var r = new PIXI.autoDetectRenderer(e.stageWidth, e.stageHeight, {
                    transparent: !0
                }),
                a = new PIXI.Container,
                i = new PIXI.Container,
                o = new PIXI.Sprite.fromImage(e.displacementImage),
                n = new PIXI.filters.DisplacementFilter(o);
            if (this.initPixi = function() {
                    document.getElementById("liquid").appendChild(r.view), a.addChild(i), a.interactive = !0, !0 === e.fullScreen ? (r.view.style.objectFit = "cover", r.view.style.width = "100%", r.view.style.height = "100%", r.view.style.top = "0", r.view.style.left = "0", r.view.style.webkitTransform = "scale(1)", r.view.style.transform = "scale(1)") : (r.view.style.maxWidth = "100%", r.view.style.top = "0", r.view.style.left = "0", r.view.style.webkitTransform = "none", r.view.style.transform = "none"), o.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT, a.filters = [n], !1 === e.autoPlay && (n.scale.x = 0, n.scale.y = 0), !0 === e.wacky && (o.anchor.set(.5), o.x = r.width / 2, o.y = r.height / 2), o.scale.x = 2, o.scale.y = 2, n.autoFit = e.displaceAutoFit, a.addChild(o)
                }, this.loadPixiSprites = function(t) {
                    for (var a = e.sprites, o = 0; o < a.length; o++) {
                        var n = new PIXI.Texture.fromImage(t[o]),
                            s = new PIXI.Sprite(n);
                        !0 === e.centerSprites && (s.anchor.set(.5), s.x = r.width / 2, s.y = r.height / 2), i.addChild(s)
                    }
                }, !0 === e.autoPlay) {
                var s = new PIXI.ticker.Ticker;
                s.autoStart = e.autoPlay, s.add(function(t) {
                    o.x += e.autoPlaySpeed[0] * t, o.y += e.autoPlaySpeed[1], r.render(a)
                })
            } else {
                var l = new PIXI.ticker.Ticker;
                l.autoStart = !0, l.add(function(e) {
                    r.render(a)
                })
            }
            this.init = function() {
                t.initPixi(), t.loadPixiSprites(e.pixiSprites)
            }, this.init()
        };
        var i = (0, r.getBrowser)(),
            o = [];
        o.push("https://dev.rangka.id/static/bg-wave.4e73f6ad.png");
        var n = new CanvasSlideshow({
            sprites: o,
            displacementImage: "https://raw.githubusercontent.com/Pierrinho/elephant/master/pattern-clouds.jpg",
            autoPlay: !0,
            autoPlaySpeed: [0, 3],
            interactive: !1,
            interactionEvent: "click",
            displaceAutoFit: !1,
            dispatchPointerOver: !0
        });
        window.onload = function() {
            var e = document.getElementById("liquid"),
                r = document.querySelector("#liquid canvas"),
                a = document.querySelectorAll(".word-wrap > .word"),
                o = (document.querySelector(".word-wrap.line"), document.querySelector(".banner")),
                n = document.querySelector(".button--delay"),
                s = document.querySelector(".drawer"),
                l = document.querySelector(".drawer__wrapper"),
                c = (document.querySelector(".with-hand"), require("./paraxify"), document.querySelectorAll(".work__cover")),
                d = document.querySelectorAll(".work--boxed"),
                u = document.querySelector(".work__box.d-lg-flex"),
                p = 0,
                w = 0;
            if (c.length > 0) new t.default(c);
            if (d.length > 0 && d.forEach(function(e, t) {
                    if (t < 2) {
                        var r = e.clientHeight,
                            a = window.getComputedStyle(e),
                            i = parseInt(a.marginTop, 10);
                        p += r + 2 * i
                    }
                }), u) {
                var y = u.clientHeight;
                window.getComputedStyle(u);
                w = y + 64
            }
            "Desktop" == i.device && setTimeout(function() {
                var e = p + w;
                console.log("tinggi ", e), document.querySelector(".intro__works-recent").style.maxHeight = e + "px"
            }, 500), e && e.classList.add("is-loaded"), a && a.forEach(function(e, t) {
                e.classList.add("loaded"), e.classList.add("loaded-" + t)
            }), n && n.classList.add("loaded"), o && o.addEventListener("mousemove", function(e) {
                var t = (window.innerWidth - 2 * e.pageX) / 100,
                    a = (window.innerHeight - 2 * e.pageY) / 100;
                r.style.transform = "scale(1) translateX(".concat(t, "px) translateY(").concat(a, "px)")
            }), s.onclick = function() {
                var e = document.body;
                e.classList.contains("menu-is-open") ? (e.classList.remove("menu-is-open"), l.classList.remove("is-open")) : (e.classList.add("menu-is-open"), l.classList.add("is-open"))
            }
        };
        var s = 0,
            l = document.querySelector(".app__wrapper");
        window.onscroll = function(e) {
            var t = document.body.scrollTop || document.documentElement.scrollTop,
                r = document.querySelector(".navbar");
            t > s ? r.classList.contains("scrolled-up") ? r.classList.remove("scrolled-up") : r.classList.add("scrolled-down") : (r.classList.contains("scrolled-down") ? r.classList.remove("scrolled-down") : r.classList.add("scrolled-up"), document.body.scrollTop > window.outerHeight / 2 || document.documentElement.scrollTop > window.outerHeight / 2 ? r.style.backgroundColor = "#323233" : r.style.backgroundColor = "transparent"), s = t
        }, console.log("RANGKA CREATIVE");
    }, {
        "simple-parallax-js": "tOor",
        "./detect-browser": "kR2E",
        "./paraxify": "baIT"
    }]
}, {}, ["JAKd"], null)
//# sourceMappingURL=https://dev.rangka.id/../build/web.234a786d.js.map